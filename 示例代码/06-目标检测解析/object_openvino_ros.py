#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy,os
from std_msgs.msg import String
from ros_openvino.msg import Objects
from sensor_msgs.msg import RegionOfInterest as ROI
rospy.init_node('wrc_objects', anonymous=False)
spub=rospy.Publisher("/object_data",String,queue_size=1)
roi_pub=rospy.Publisher("/roi",ROI,queue_size=5)
objdata=String()
roi_data=ROI()
count=0
last_object=''

def callback(data):
	global count
	global last_object
	data = data.objects
	if data:        
		for i in data:		
			p = i.confidence
			object_name = i.label
			roi_data=ROI()
			roi_data.x_offset=int(i.x*640)
			roi_data.y_offset=int(i.y*480)
			roi_data.width=int(i.width*640)
			roi_data.height=int(i.height*480)
			if p >= 0.95:
				new_object=object_name
				if new_object==last_object:
					count+=1				
				if count>=10:
					if object_name != "person" and object_name != "tvmonitor":
						roi_pub.publish(roi_data)
						spub.publish(object_name)
					count=0

				last_object=new_object			
    
def object_search():
	rospy.Subscriber("/object_detection/results", Objects, callback, queue_size=1)
	rospy.spin()
if __name__ == '__main__':
	print("物体识别解析")
	try:
		object_search()
	except rospy.ROSInterruptException:
		rospy.loginfo("object node terminated.")
    
