#!/usr/bin/env python
import rospy
from aikit_arm.srv import aikit_arm_srv, aikit_claw_srv
rospy.wait_for_service("aikit_arm")
rospy.wait_for_service("aikit_claw")

arm = rospy.ServiceProxy("aikit_arm", aikit_arm_srv)
claw = rospy.ServiceProxy("aikit_claw", aikit_claw_srv)

if __name__ == '__main__':
    while not rospy.is_shutdown():
        for i in range(20,80):
            arm(i)
            claw(100-i)
            rospy.sleep(0.02)
            rospy.loginfo("arm_pos", i)
            rospy.loginfo("claw_pos", 100 - i)
        for i in range(80,20,-1):
            arm(i)
            claw(100-i)
            rospy.sleep(0.02)
            rospy.loginfo("arm_pos", i)
            rospy.loginfo("claw_pos", 100 - i)

