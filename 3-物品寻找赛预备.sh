#!/usr/bin/env bash
source /opt/intel/openvino/bin/setupvars.sh
source ~/aikit_ws/devel/setup.bash
gnome-terminal  "启动aikit底盘" -x bash -c " roslaunch aikit_chassis drive_start.launch;exec bash  "
sleep 3
gnome-terminal -x bash -c " roslaunch aikit_arm claw_service.launch;exec bash"
sleep 2
gnome-terminal  "加载地图" -x bash -c "roslaunch aikit_nav aikit_nav_teb_demo.launch;exec bash "
#gnome-terminal  "启动导航rviz" -x bash -c "roslaunch aikit_rviz amcl_view_teb.launch;exec bash "
sleep 3
gnome-terminal -x bash -c "roslaunch realsense_camera r200_nodelet_rgbd.launch;exec bash"
sleep 3
gnome-terminal -x bash -c "python ~/Desktop/wrc_demo/src/qrcode_node.py;exec bash"
sleep 4
gnome-terminal -x bash -c " roslaunch ros_openvino myriad_realsense_r200.launch;exec bash"
sleep 3
gnome-terminal -x bash -c "python ~/Desktop/wrc_demo/src/object_node.py;exec bash"

